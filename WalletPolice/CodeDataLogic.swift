//
//  CodeDataLogic.swift
//  WalletPolice
//
//  Created by Dmitriy Zaretskiy on 21.08.17.
//  Copyright © 2017 Bubble. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataLogic {
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }

    func storeUser (passwd: String) {
        let context = getContext()
        let entity =  NSEntityDescription.entity(forEntityName: "Entity", in: context)
        let user = NSManagedObject(entity: entity!, insertInto: context)
        user.setValue(passwd, forKey: "passwd")
        do {
            try context.save()
            print("saved!")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        } catch {
        }
    }
    
    
    func getUsers () ->[String]{
        var some:[String]? = Array()

        let fetchRequest: NSFetchRequest<Entity> = Entity.fetchRequest()
        
        do {
            let array_users = try getContext().fetch(fetchRequest)
            for user in array_users as [NSManagedObject] {
                some?.append(user.value(forKey: "passwd").debugDescription)                
            }
                     return some!;
           
        } catch {
            print("Error with request: \(error)")
                     return some!;
        }

    }
    
    func DeleteAll()
    {
        
    }
    
}
