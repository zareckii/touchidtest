//
//  TouchIDViewController.swift
//  WalletPolice
//
//  Created by Dmitriy Zaretskiy on 21.08.17.
//  Copyright © 2017 Bubble. All rights reserved.
//

import UIKit

class TouchIDViewController: UIViewController {

    @IBOutlet weak var touchIdStatus: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var info = segue.destination as! ViewController
        info.swich = touchIdStatus.isOn
    }
    

}
