//
//  AddViewController.swift
//  WalletPolice
//
//  Created by Dmitriy Zaretskiy on 21.08.17.
//  Copyright © 2017 Bubble. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {

    
    @IBOutlet weak var addPasswd: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addButton(_ sender: UIButton) {
        let coreData:CoreDataLogic = CoreDataLogic()
        if addPasswd.text != ""{
            coreData.storeUser(passwd: addPasswd.text!);
            performSegue(withIdentifier: "goToStart", sender: nil)
            
        }
        else{
            //тут будет alert, но мне пока лень его писать
        }
    }
   
   

   
}
