//
//  ViewController.swift
//  WalletPolice
//
//  Created by Dmitriy Zaretskiy on 21.08.17.
//  Copyright © 2017 Bubble. All rights reserved.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController {
    
    var swich:Bool? = nil
    
    @IBOutlet weak var somePasswd: UITextField!
    @IBOutlet weak var LableStatus: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func ClickThisButton(_ sender: UIButton) {
        if swich == true{
            LableStatus.text = "Use TouchID"
            login_in_password()
            
        }
        else
        {
            login_in_password()
            swich = false
        }
    }
    
    @IBAction func TouchIDButton(_ sender: UIButton) {
        login_in_touchID()
    }
    
    private func login_in_password()
    {
        let coreData:CoreDataLogic = CoreDataLogic()
        // let list:[String] = coreData.getUsers()
        print(coreData.getUsers())
        for index in coreData.getUsers(){
            if index == "Optional("+somePasswd.text!+")"{
                performSegue(withIdentifier: "Some", sender: nil)
                break
            }
        }
    }
    
    private func login_in_touchID()
    {
        LableStatus.text = "Use TouchID"
        
        let context:LAContext = LAContext();
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        {
            context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "We need your TouchID", reply: {
                (wasSuccessful, error) in
                if wasSuccessful{
                    DispatchQueue.main.async(execute:{
                    self.performSegue(withIdentifier: "Some", sender: nil)
                    })
                    print("Успех")
                }
                else{
                    self.LableStatus.text = "Error"
                }
            })
        }
    }

}

